<!DOCTYPE html>
<html lang="es">
<head>
    <link rel="shortcut icon" href="http://placehold.it/64.png"/>
<meta charset="UTF-8">
<title> <?= empty($title)?'Inicio | Pantuflas Finas':$title ?> </title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" crossorigin="anonymous">
<link rel="stylesheet" href="<?= base_url() ?>css/template/custom.css">

<?php if($this->router->fetch_method()!='editor'): ?>
<script src="https://code.jquery.com/jquery-3.2.1.min.js" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" crossorigin="anonymous"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
<script src="https://unpkg.com/scrollreveal/dist/scrollreveal.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/elevatezoom/3.0.8/jquery.elevatezoom.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/head.js" defer="defer"></script>
<script src="https://www.google.com/recaptcha/api.js?hl=es&onload=renderCaptcha&render=explicit" async="async" defer="defer"></script>
    <script>
        var recaptcha;
        var renderCaptcha = function() {
            recaptcha = grecaptcha.render('recaptcha', {
                'sitekey': '6LfhZE4UAAAAAGfSjcpK86Ci8WULqAx80vkdFRN8',
                'theme': 'dark'
            });
        };
    </script>
<?php 
    if(!empty($css_files) && !empty($js_files)):
    foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?= $file ?>" />
    <?php endforeach; ?>
    <?php foreach($js_files as $file): ?>
    <script src="<?= $file ?>"></script>
    <?php endforeach; ?>                
    <?php endif; ?>
<?php endif ?>
</head>
<body>
    
    <?php $this->load->view('includes/template/header'); ?>
    <?php $this->load->view($view); ?>
    <?php $this->load->view('includes/template/footer'); ?>
    <script src="<?= base_url() ?>js/template/foot.js"></script>
    <script>
        $("#minus-item").click(function(e){
            if( parseInt( $("#qty-product").val() )>1 )
                $("#qty-product").val( parseInt( $("#qty-product").val() )-1 );
        });

        $("#plus-item").click(function(e){
                $("#qty-product").val( parseInt( $("#qty-product").val() )+1 );
        });
    </script>
    </body>
</html>