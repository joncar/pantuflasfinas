<nav class="navbar navbar-expand-md navbar-light bg-light">
    <div class="absoluter bg-yellow-light bg-navbar-01 col-8 col-sm-4 h-100"></div>
    <div class="absoluter bg-black bg-navbar-02 col-4 col-sm-8 h-100"></div>
    <div class="container nb-container m-auto relativer">
        <a class="navbar-brand" href="http://www.pantuflasfinas.com.mx/">
            <!-- <img src="http://placehold.it/200x50.jpg&text=200x50" alt="Brand" class="img-responsive"> -->
            <h2>PANTUFLAS FINAS</h2>
        </a>
        <button class="navbar-toggler navbar-dark" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul id="parallax-navbar" class="navbar-nav mr-auto">
                    <li class='nav-item'>
                        <a class='nav-link text-center text-white' href='<?= base_url() ?>#nosotros' data-target="<?= $this->router->fetch_class()!='frontend'?'#nosotros':'no-parallax' ?>">NOSOTROS</a>
                    </li>
                    <li class='nav-item'>
                        <a class='nav-link text-center text-white' href='<?= base_url() ?>#catalogo' data-target="<?= $this->router->fetch_class()!='frontend'?'#catalogo':'no-parallax' ?>">CATÁLOGO</a>
                    </li>
                    <li class='nav-item'>
                        <a class='nav-link text-center text-white' href='<?= base_url('pedidos') ?>' data-target="no-parallax">PEDIDOS</a>
                    </li>
                    <li class='nav-item'>
                        <a class='nav-link text-center text-white' href='<?= base_url() ?>#contacto' data-target="<?= $this->router->fetch_class()!='frontend'?'#contacto':'no-parallax' ?>">CONTACTO</a>
                    </li>
                    <li class='nav-item'>
                        <a class='nav-link text-center text-white' href='<?= base_url('carrito') ?>' data-target="no-parallax">CARRITO</a>
                    </li>
            </ul>
        </div>
    </div>
</nav>