<?php if($this->user->log): ?>
<div id="sidebar" class="sidebar responsive">
        <ul class="nav nav-list">
            <li class="active highlight">
                <a href="<?= site_url('panel') ?>">
                        <i class="menu-icon fa fa-tachometer"></i>
                        <span class="menu-text">Escritorio</span>
                </a>
                <b class="arrow"></b>
            </li>
             <!--- Alumnos --->
             <?php 
                    $menu = array(                        
                        'catalogo'=>array('admin/catalogo','admin/importador_catalogo'),
                        'solicitud_web'=>array('admin/solicitud_web'),
                        'contacto_web'=>array('admin/contacto_web'),                        
                        'reportes'=>array(
                            'rep/newreportes',
                            'rep/report_organizer',
                            'rep/ver_reportes',
                            'rep/mis_reportes',
                        ),
                        'mantenimiento'=>array(                            
                            'admin/pedidos',
                            'admin/productos',    
                            'admin/tipos_telas',
                            'admin/temporadas',
                            'admin/contactenos'
                        ),
                        'notificaciones'=>array('admin/notificaciones'),
                        'pagina_web'=>array('paginas','ajustes_generales'),
                        'seguridad'=>array('acciones','ajustes','grupos','funciones','user')
                    );
                    $menu = $this->user->filtrarMenu($menu);
                    $label = array(
                        'b'=>array('Blog','fa fa-book'),
                        'productos'=>array('Catalogo'),
                        'solicitud_web'=>array('Pedidos'),
                        'habitacion'=>array('Habitaciones','fa fa-bed'),
                        'notificaciones'=>array('Notificaciones','fa fa-bullhorn'),
                        'grupos_destinos'=>array('Grupos','fa fa-group'),
                        'paginas'=>array('Paginas','fa fa-file-powerpoint-o'),                        
                        'seguridad'=>array('Seguridad','fa fa-user-secret')
                    );
             ?>
             <?php  echo getMenu($menu,$label); ?>            
        </ul>
       <div id="sidebar-collapse" class="sidebar-toggle sidebar-collapse">
            <i data-icon2="ace-icon fa fa-angle-double-right" data-icon1="ace-icon fa fa-angle-double-left" class="ace-icon fa fa-angle-double-left"></i>
        </div>
        <div style="color:white; background:#1C1B1A; font-size:8px; text-align:center">
            <a href="http://sistembux.com/" style="color:white;">
                <?= img('img/minilogo.png','width:30%') ?>
            </a>
        </div>
        <script type="text/javascript">
                try{ace.settings.check('sidebar' , 'collapsed')
                ace.settings.sidebar_collapsed(true, true);
                }catch(e){}
        </script>
</div>
<?php endif ?>
