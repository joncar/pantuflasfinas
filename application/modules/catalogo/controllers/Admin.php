<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function catalogo($x = '',$y = ''){
        	$this->as['catalogo'] = 'productos';
            $crud = $this->crud_function($x,$y);            
            $crud->set_relation_n_n('tallas','productos_tallas','tallas','productos_id','tallas_id','{nombre}');            
            $crud->field_type('colores_tela','set',array());
            $crud->field_type('colores','tags');
            $crud->field_type('genero','dropdown',array('M'=>'Masculino','F'=>'Femenino'));
            $crud->display_as('Colores disponibles');
            if($crud->getParameters()!='list'){
                $crud->display_as('foto','Foto de listado (Tamaño recomendado 1000x600px)');
            }
            $crud->order_by('estilo','ASC');
            $crud->columns('foto','estilo','genero','temporadas_id');
            $crud->add_action('<i class="fa fa-image"></i> Adm Fotos','',base_url('productos/admin/productos_fotos').'/');
            $crud->set_field_upload('foto','img/productos');
            $crud->display_as('colores','Colores disponibles');
            $output = $crud->render();
            if(is_numeric($y)){
                $colores = @$this->db->get_where('productos',array('id'=>$y))->row()->colores;
            }else{
                $colores = '';
            }            
            $output->output = $this->load->view('productos',array('output'=>$output->output,'colores'=>$colores),TRUE,'mantenimiento');
            $this->loadView($output);
        } 

        function productos_fotos($x = '',$y = ''){
            $this->load->library('image_crud');
            $crud = new image_crud();
            $crud->set_table('productos_fotos')
                 ->set_image_path('img/productos')
                 ->set_url_field('foto')
                 ->set_relation_field('productos_id')
                 ->set_ordering_field('orden');
            $crud->module = 'productos';
            $output = $crud->render();              
            $this->loadView($output);
        } 

        function importador_catalogo(){
            $this->as['importador_catalogo'] = 'importador';
            $crud = $this->crud_function('','');                        
            $crud->field_type('temporada','enum',array('VERANO','OTOÑO','PRIMAVERA','INVIERNO'));
            $crud->field_type('remplazar','true_false',array('1'=>'SI','0'=>'NO'));
            $crud->set_field_upload('fichero','files')
                 ->set_field_upload('fotos','files');     
            $crud->display_as('fichero','Fichero de productos(Formato permitido XLS)')
                 ->display_as('fotos','Fichero de fotos(Formato permitido ZIP [estilo_id])')
                 ->display_as('anio','Año');
            if($crud->getParameters()=='list' && $crud->getParameters(false)!='success'){
                $crud->set_lang_string('insert_success_message','Su fichero ha sido almacenado, deseas procesarlo? <a href="'.base_url('productos/admin/procesar_fichero').'/{primary}">Pulsa aqui</a> |');
            }
            $crud->add_action('<i class="fa fa-file"></i> Procesar Fichero','',base_url('catalogo/admin/procesar_fichero').'/');
            $crud->add_action('<i class="fa fa-image"></i> Procesar Fotos','',base_url('catalogo/admin/procesar_fotos_ftp').'/');
            $output = $crud->render();
            $this->loadView($output);
        }

        function procesar_fotos($id){
            if(is_numeric($id)){
                $fichero = $this->db->get_where('importador',array('id'=>$id));
                if($fichero->num_rows()>0){
                    $fichero = $fichero->row();
                    $zip = new ZipArchive;
                    $res = $zip->open('files/'.$fichero->fotos);
                    if ($res === TRUE) {
                      $zip->extractTo('img/productos');
                      $zip->close();
                      $fotos = scandir('img/productos');
                      foreach($fotos as $f){
                        if($f!='.' && $f!='..'){
                            $ff = explode('_',$f);
                            if(is_numeric($ff[0])){
                                $producto = $this->db->get_where('productos',array('estilo'=>$ff[0]));
                                if($producto->num_rows()>0){
                                    if(empty($producto->row()->foto)){
                                        $this->db->update('productos',array('foto'=>$f),array('estilo'=>$ff[0]));
                                    }else{      
                                        if($this->db->get_where('productos_fotos',array('foto'=>$f,'productos_id'=>$producto->row()->id))->num_rows()==0){                                  
                                            $this->db->insert('productos_fotos',array(
                                                'foto'=>$f,
                                                'productos_id'=>$producto->row()->id,
                                                'orden'=>0
                                            ));
                                        }
                                    }
                                }else{
                                    unlink('img/productos/'.$f);
                                }
                            }
                        }
                      }
                      echo 'woot!';
                    } else {
                      echo 'doh!';
                    }
                }
            }
        }

        function procesar_fichero($id){
            if(is_numeric($id)){
                $fichero = $this->db->get_where('importador',array('id'=>$id));
                if($fichero->num_rows()>0){
                    $fichero = $fichero->row();
                    require_once APPPATH."libraries/Excel/SpreadsheetReader.php";
                    $excel = new SpreadsheetReader('files/'.$fichero->fichero);
                    $almacenados = 0;
                    $total = 0;
                    foreach ($excel as $Row)
                    {                        
                        $genero = $Row[26]=='DAMA'?'F':'M';
                        $precio = str_replace('$','',$Row[2]);
                        $precio = str_replace(',','.',$precio);
                        if(is_numeric($precio)){
                            $total++;                            
                            $data = array(
                                'temporadas_id'=>$fichero->temporadas_id,
                                'genero'=>$genero,
                                'nombre'=>$Row[1],
                                'estilo'=>$Row[0],
                                'precio'=>$precio,
                                'colores'=>$Row[27],
                                'estatus'=>0,
                                'origen'=>$Row[5],
                                'linea'=>$Row[3],
                                'lineal'=>$Row[4]
                            );                            
                            if(isset($Row[28]) && is_numeric($Row[28]))$data['tipos_telas_id'] = $Row[28];
                            if(isset($Row[29]) && is_numeric(explode(',',$Row[29])[0]))$data['colores_tela'] = str_replace('.',',',$Row[29]);
                            $almacenar_tallas = false;
                            if($fichero->eliminar_productos){
                                $this->db->delete('productos',array('temporadas_id'=>$fichero->temporadas_id));
                            }
                            $existe = $this->db->get_where('productos',array('temporadas_id'=>$data['temporadas_id'],'estilo'=>$data['estilo']));
                            if($existe->num_rows()>0 && $fichero->reemplazar==1){
                                $this->db->update('productos',$data,array('temporadas_id'=>$data['temporadas_id'],'estilo'=>$data['estilo']));
                                $producto = $existe->row()->id;
                                $almacenados++;
                                $this->db->delete('productos_tallas',array('productos_id'=>$existe->row()->id));
                                $almacenar_tallas = true;
                            }
                            if($existe->num_rows()==0){
                                $this->db->insert('productos',$data);
                                $producto = $this->db->insert_id();
                                $almacenados++;
                                $almacenar_tallas = true;
                            }                            
                            if($almacenar_tallas){
                                for($i=6;$i<=25;$i++){
                                    if(is_numeric($Row[$i])){
                                        $Row[$i] = str_replace('.',',',$Row[$i]);
                                        $tallaid = $this->db->get_where('tallas',array('nombre'=>$Row[$i]));
                                        if($tallaid->num_rows()>0){
                                            $this->db->insert('productos_tallas',array('productos_id'=>$producto,'tallas_id'=>$tallaid->row()->id));
                                        }
                                    }
                                }
                            }
                        }                        
                    }
                    //$this->procesar_fotos($id);
                    echo 'Registros insertados '.$almacenados.' de '.$total.' <a href="'.base_url('catalogo/admin/importador_catalogo').'">Volver a la lista</a>';
                }
            }
        }

        function procesar_fotos_ftp($id){
            if(is_numeric($id)){
                $fichero = $this->db->get_where('importador',array('id'=>$id));
                if($fichero->num_rows()>0){
                    $fichero = $fichero->row();
                    $this->db->update('productos',array('foto'=>''),array('temporadas_id'=>$fichero->temporadas_id));
                    foreach($this->db->get_where('productos',array('temporadas_id'=>$fichero->temporadas_id))->result() as $p){
                        $this->db->delete('productos_fotos',array('productos_id'=>$p->id));
                    }
                    
                    $files = scandir('img/productos');
                    foreach($files as $f){
                        if($f!='.' && $f!='..'){
                            $filename = $f;
                            $name = explode('.',$f,2);
                            if(count($name)==2){
                                $name = explode('_',$name[0]);
                                if(count($name)==2 && is_numeric($name[0]) && is_numeric($name[1])){
                                    $producto = $this->db->get_where('productos',array('estilo'=>$name[0],'temporadas_id'=>$fichero->temporadas_id));
                                    if($producto->num_rows()>0){
                                        if(empty($producto->row()->foto)){
                                            $this->db->update('productos',array('foto'=>$filename),array('id'=>$producto->row()->id));
                                        }else{
                                            $this->db->insert('productos_fotos',array('productos_id'=>$producto->row()->id,'foto'=>$filename,'orden'=>0));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //redirect('mantenimiento/admin/importador/success');
            }
        }
    }
?>
