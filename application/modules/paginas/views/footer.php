<footer class="pt-3 pt-md-5 pb-3 pb-md-5 bg-black">
    <div class="container-custom">
        <div class="row">
            <div class="col-sm-12 text-center">
                <div class="text-white">
                    <p class="text-white mb-3">
  Av. Tezozomoc 106 <span class="ml-3 mr-3">|</span> Col. San Miguel Amantla <span class="ml-3 mr-3">|</span> CDMX <span class="ml-3 mr-3">|</span> Municipio Azcapotzalco <span class="ml-3 mr-3">|</span> C.P. 02700
</p>
<p class="text-white mb-3">
  Tel. <a href="tel:5552502999" class="text-white">(55) 52502999</a>
</p>
<p class="text-white">
  <a href="http://test.pantuflasfinas.com.mx/p/aviso-privacidad" class="text-white">AVISO DE PRIVACIDAD</a>
</p>                </div>
                
                <div class="social">
                    <a href="https://fb.com" class="text-white mr-3" target="_blank">
                        <i class="fab fa-facebook fa-2x"></i>
                    </a>
                    <a href="https://instagram.com" class="text-white" target="_blank">
                        <i class="fab fa-instagram fa-2x"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</footer>