<div id="pedidos-carousel" class="carousel" data-ride="carousel">
	  <div class="carousel-inner">
	    <div class="carousel-item active" style="background-image: url('<?= base_url() ?>img/pedidos.png')">
	      <!-- <img class="d-block w-100" src="http://www.pantuflasfinas.com.mx/uploads/pedidos.png" alt="Cover"> -->
	    </div>
	  </div>
	</div>

	<section class="pt-60 pb-60">
	<div class="container-custom">
		<div class="row align-items-center">
			<div class="col-md-12" style="border-bottom: 2px solid #b2b2b2;">


					<table class="table">
						<tr>
							<th>Eliminar</th>
							<th>Imagen</th>
							<th>Producto</th>
							<th>Cantidad</th>
							<th>Precio</th>
						</tr>
						[foreach:carrito]
						<tr>
							<td><a href="[del_link]">Eliminar</a></td>
							<td><img src="[foto]" style="width:150px; height:auto"></td>
							<td>[estilo] Talla: [talla]</td>
							<td>[cantidad] <a href="[link]">Cambiar</a></td>
							<td>[precio]</td>
						</tr>
						[/foreach]	
					</table>
					[emptycarmessage]					

				</div>
			</div>
			<div class="row mt-45">
				<div class="col-md-4">
					<a href="[pedidoslink]" class="btn btn-yellow-light btn-noradius btn-block">Agregar más productos</a>
				</div>

				<div class="col-md-4 ml-auto bg-yellow-light pt-60 pb-60">
					<div class="row align-items-center h-100">
						<div class="col-md-12 text-center">
							<h1 class="mb-3"><strong>TOTAL</strong></h1>
							<h3> $ <span data-total> [total] </span> </h3>
						</div>
					</div>
				</div>
			</div>
			<div class="row mt-45">
				<div class="col-md-12">
					<h3 class="mb-3">Enviar pedido.</h3>
					[response]
					<form action="[formpedidosendlink]" method="POST">						
						<div class="row">
							<div class="col-md-6 mb-3 mb-md-0">
								<div class="row h-100">
									<div class="col-md-12 align-self-start">
										<div class="form-group">
											<input type="text" class="form-control" name="nombre" value="" placeholder="Nombre:" required>
										</div>
									</div>
									<div class="col-md-12 align-self-center">
										<div class="form-group">
											<input type="text" class="form-control" name="telefono" value="" placeholder="Teléfono:" required>
										</div>
									</div>
									<div class="col-md-12 align-self-center">
										<div class="form-group">
											<input type="email" class="form-control" name="email" value="" placeholder="E-Mail:" required>
										</div>
									</div>
									<div class="col-md-12 align-self-end">
										<input type="text" class="form-control" name="empresa" value="" placeholder="Empresa:" required>
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<textarea class="form-control" name="mensaje" rows="5" placeholder="Mensaje:"></textarea>
								</div>

								<div class="row">
									<div class="col-md-6 mb-3 mb-md-3">
										<div id="recaptcha"></div>
									</div>
									<div class="col-md-6 align-self-end">
										<button type="submit" class="btn btn-black btn-noradius pl-5 pr-5 float-sm-right">Enviar</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>


	<a href="https://api.whatsapp.com/send?phone=525500000000" class="whatsapp-contact p-2 p-md-3">
		<span class="d-none d-md-block">Envíanos un mensaje de Whatsapp</span>
		<i class="fab fa-whatsapp fa-2x d-block d-md-none"></i>
	</a>