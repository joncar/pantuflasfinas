<div id="pedidos-carousel" class="carousel" data-ride="carousel">

		<div class="carousel-inner">

			<div class="carousel-item active" style="background-image: url('<?= base_url() ?>img/pedidos.png')">

				<!-- <img class="d-block w-100" src="http://placehold.it/1900x800.jpg?text=1900x800.jpg" alt="Cover"> -->

			</div>

		</div>

	</div>



	<section class="bg-yellow-light pt-60 pb-60">

		<div class="container-custom">

			<div class="pedidos-title col-md-4">

				<h2 class="mb-3">CATÁLOGO <br class="d-none d-md-block"> DE PRODUCTOS</h2>

				<p>

					<a class="text-black" href="<?= base_url('pedidos/caballeros') ?>">CABALLEROS</a>

					<span class="ml-3 mr-3">|</span>

					<a class="text-black" href="<?= base_url('pedidos/damas') ?>">DAMAS</a>

				</p>

			</div>



			<form class="row pt-30 align-items-center products-container" action="[form_url]" method="POST">

					<div class="col-md-6 mb-3 mb-md-0 main-image-container" style="align-self:flex-start;">

						<img id="zoom-image" class="img-fluid d-block m-auto" src="[foto]">
						<div class="row mt-3" id="gallery_01">

							[foreach:fotos]

							<div class="col-md-4 mb-3 mb-md-0">

								<a href="#" data-img="1" data-zoom-image="[foto]" data-image="[foto]">

									<img class="img-fluid d-block m-auto" src="[foto]" alt="[nombre]">

								</a>

							</div>

							[/foreach]

							



							<script>

								//initiate the plugin and pass the id of the div containing gallery images

								$("#zoom-image").elevateZoom({

									gallery:'gallery_01',

									cursor: 'crosshair',

									galleryActiveClass: 'active',

									imageCrossfade: true,

									loadingIcon: 'http://www.elevateweb.co.uk/spinner.gif',

									zoomType: "inner",

									lensSize: 100,

									scrollZoom: true

								}); 



								//pass the images to Fancybox

								$("#zoom-image").bind("click", function(e) {  

								  var ez =   $('#zoom-image').data('elevateZoom');	

									$.fancybox(ez.getGalleryList());

								  return false;

								});								

							</script>

						</div>
					</div>

					<div class="col-md-6">

						[msj]

						<p><strong>DESCRIPCIÓN</strong></p>

						<p>

							<strong>[nombre]</strong>

						</p>

						<p>

							<strong>ESTILO:</strong> [estilo]</p>
						

						<p>

							<strong>Precio:</strong> $ [precio]	</p>

						<p>

							<strong>Origen:</strong> [origen]</p>

						

						<div class="row mt-3 colors-container">

							<div class="col-md-12">

								<p><strong>Colores disponibles:</strong></p>

							</div>
							<div class="col-md-12 mb-3">[colores]</div>						

							[telas]

							



							<div class="col-md-12 mb-3">

								<div class="row mb-3">

									<div class="col-md-12" style="margin-bottom: 20px">

										<strong>TALLA:</strong>

										[tallas]

									</div>

									<div class="col-md-6">


										<div class="input-group">

											<div class="input-group-prepend">

												<button id="minus-item" type="button" class="btn btn-outline-secondary">

													<i class="fas fa-minus-circle fa-lg"></i>

												</button>

											</div>

											<input id="qty-product" class="form-control text-center" name="cantidad" value="1" min="1" required>

											<div class="input-group-append">

												<button id="plus-item" type="button" class="btn btn-outline-secondary">

													<i class="fas fa-plus-circle fa-lg"></i>

												</button>

											</div>

										</div>

									</div>

								</div>								

								<input type="hidden" name="id" value="[id]">

								<input type="hidden" name="precio" value="[precio]">
								<div id="hiddens"></div>

								<button type="button" id="addPedido" class="btn btn-black btn-noradius">Agregar producto</button>

							</div>

							<div class="itemsaddeds col-md-12 mb-3" style="display: none">
								<p><b>Añadidos</b></p>
							</div>
							<div class="col-md-12 mb-3 itemsaddeds " style="display: none">
								<table class="table table-bordered" style="background:white;">
									<thead>
										<tr>
											<th>Cantidad</th>
											<th>Talla</th>
											<th style="text-align: center">x</th>
										</tr>
									</thead>
									<tbody id="itemsProducts">
										
									</tbody>
								</table>
								<button type="submit" id="SendButtonSubmit" disabled="" class="btn btn-black btn-noradius">Añadir al carro</button> (Minimo permitido 60 productos)
							</div>

						</div>





						

					</div>

			</form>

		</div>

	</section>

	



	[contacto]