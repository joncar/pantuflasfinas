
            
            <div id="home-carousel" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner">
        <div class="carousel-item active" style="background-image:url('http://test.pantuflasfinas.com.mx/img/01.jpg') !important;">
          <!-- <img class="d-block w-100" src="http://placehold.it/1900x900.jpg?text=1900x900.jpg" alt="Cover"> -->
          <div class="carousel-caption text-center">
            <h1 class="text-white" id="mce_20">UNA VIDA CONFORTABLE ES UNA VIDA FELIZ</h1>
                      </div>
        </div>
        <div class="carousel-item" style="background-image:url('http://test.pantuflasfinas.com.mx/img/02.jpg') !important;">
          <!-- <img class="d-block w-100" src="http://placehold.it/1900x900.jpg?text=02 1900x900.jpg" alt="Cover"> -->
        </div>
        <div class="carousel-item" style="background-image:url('http://test.pantuflasfinas.com.mx/img/03.jpg') !important;">
          <!-- <img class="d-block w-100" src="http://placehold.it/1900x900.jpg?text=03 1900x900.jpg" alt="Cover"> -->
        </div>
      </div>
    </div>

    <section class="pt-60 pb-60 bg-gray-light">
        <div class="container-custom">
            <div class="row">
                <div class="col-md-12">
                    <h1 id="nosotros" class="mb-3 text-center"><strong>NOSOTROS</strong></h1>
                    <div class="text-justify">
                        <p class="text-justify mb-3" id="mce_21"><strong>Nuestra Misión:</strong></p>
<p class="text-justify mb-3" id="mce_22">Proporcionar a nuestros clientes locales y foráneos una extensa variedad de pantuflas, con desempeño y servicio superiores al costo más competitivo para los usuarios (clientes).</p>                    </div>
                    
                    <div class="row justify-content-center align-items-center mb-45">
                        <div class="col-md-2 mb-3 md-md-0 editablesection" id="mce_23"><img class="img-fluid d-block m-auto" src="http://test.pantuflasfinas.com.mx/img/liverpool.png" alt="liverpool.png"></div>
                        <div class="col-md-2 mb-3 md-md-0 editablesection" id="mce_24"><img class="img-fluid d-block m-auto" src="http://test.pantuflasfinas.com.mx/img/palacio.png" alt="palacio.png"></div>
                        <div class="col-md-2 mb-3 md-md-0 editablesection" id="mce_25"><img class="img-fluid d-block m-auto" src="http://test.pantuflasfinas.com.mx/img/chedraui.png" alt="chedraui.png"></div>
                        <div class="col-md-2 mb-3 md-md-0 editablesection" id="mce_26"><img class="img-fluid d-block m-auto" src="http://test.pantuflasfinas.com.mx/img/2018-04-04_230715_2018-04-02_141856_comer_fresko_logo.png" alt="2018-04-04_230715_2018-04-02_141856_comer_fresko_logo.png"></div>
                        <div class="col-md-2 mb-3 md-md-0 editablesection" id="mce_27"><img class="img-fluid d-block m-auto" src="http://test.pantuflasfinas.com.mx/img/2018-04-04_230715_soriana logo.png" alt="2018-04-04_230715_soriana logo.png"></div>
                    </div>

                    <h1 id="catalogo" class="mb-3 mb-md-5 text-center"><strong>CATÁLOGO</strong></h1>
                    <div class="row justify-content-center">
                        <div class="col-md-6 mb-3 mb-md-0">
                            <a href="http://test.pantuflasfinas.com.mx/pedidos/damas" id="mce_28" class=""><img class="img-fluid d-block m-auto" src="http://test.pantuflasfinas.com.mx/img/2018-04-10_134352_fondo_dama_1_2.jpg" alt="Lady"></a>
                            <h4 class="text-center mt-3" id="mce_29">PANTUFLAS PARA DAMA</h4>
                        </div>
                        <div class="col-md-6 mb-3">
                            <a href="http://test.pantuflasfinas.com.mx/pedidos/caballeros" id="mce_30" class=""><img class="img-fluid d-block m-auto" src="http://test.pantuflasfinas.com.mx/img/2018-04-12_125216_fondo_caballero_2.jpg" alt="Gentleman"></a>
                            <h4 class="text-center mt-3" id="mce_31">PANTUFLAS PARA CABALLERO</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="mt-3 mb-3"></section><div style="text-align:center;"></div><div style="text-align:center;"></div>    
    [contacto]
        