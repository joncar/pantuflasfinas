<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Seguridad extends Panel{
        function __construct() {
            parent::__construct();
        }

        function ajustes(){
            $crud = $this->crud_function('','');
            $crud->unset_add()->unset_delete()->unset_read()->unset_export()->unset_print();
            $crud->columns('id');
            $crud->field_type('analytics','string');
            $crud = $crud->render();
            $this->loadView($crud);
        } 
        
        function grupos($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);            
            $crud->set_relation_n_n('funciones','funcion_grupo','funciones','grupo','funcion','{nombre}');
            $crud->set_relation_n_n('miembros','user_group','user','grupo','user','{nombre}');                                 
            $output = $crud->render();
            $this->loadView($output);
        }               
        
        function funciones($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);            
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function user($x = '',$y = ''){
            $this->norequireds = array('apellido_materno','foto');
            $crud = $this->crud_function($x,$y);  
            $crud->field_type('status','true_false',array('0'=>'No','1'=>'Si'));
            $crud->field_type('admin','true_false',array('0'=>'No','1'=>'Si'));
            $crud->field_type('password','password');  
            $grupos = $this->db->get_where('grupos');          
            $grupo = array();
            foreach($grupos->result() as $g){
                $grupo[] = $g->id.'-'.$g->nombre;
            }
            $crud->field_type('permisos','set',$grupo);  
            $crud->unset_fields('fecha_registro','fecha_actualizacion');
            $crud->set_field_upload('foto','img/fotos');            
            $crud->callback_before_insert(function($post){
                get_instance()->enviarcorreo((object)$post,5);
                $post['fecha_registro'] = date("Y-m-d");
                $post['fecha_actualizacion'] = date("Y-m-d");
                $post['password'] = md5($post['password']);
                return $post;
            });
            $crud->callback_after_insert(function($post,$primary){
                $permisos = $post['permisos'];
                get_instance()->db->delete('user_group',array('user'=>$primary));
                foreach($permisos as $p){
                    list($id,$grupo) = explode('-',$p);
                    $this->db->insert('user_group',array('user'=>$primary,'grupo'=>$id));
                }
            });
            $crud->callback_before_update(function($post,$primary){
                if(get_instance()->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'])
                $post['password'] = md5($post['password']);
                return $post;
            });        
            $crud->callback_after_update(function($post,$primary){
                $permisos = $post['permisos'];
                get_instance()->db->delete('user_group',array('user'=>$primary));
                foreach($permisos as $p){
                    list($id,$grupo) = explode('-',$p);
                    $this->db->insert('user_group',array('user'=>$primary,'grupo'=>$id));
                }                
            });    
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function perfil($x = '',$y = ''){
            $this->as['perfil'] = 'user';            
            $crud = $this->crud_function($x,$y);  
            $crud->field_type('password','password');  
            $crud->field_type('grupos','password');  
            $crud->field_type('permisos','invisible');  
            $crud->unset_fields('status','admin','fecha_registro','fecha_actualizacion');
            $crud->set_field_upload('foto','img/fotos');                        
            $crud->callback_before_update(function($post,$primary){
                if(get_instance()->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'])
                $post['password'] = md5($post['password']);
                return $post;
            });                    
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function user_insertion($post,$id = ''){
            if(!empty($id)){
                $post['pass'] = $this->db->get_where('user',array('id'=>$id))->row()->password!=$post['password']?md5($post['password']):$post['password'];
            }
            else $post['pass'] = md5($post['pass']);
            return $post;
        }
    }
?>
