<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }

        function tallas(){
            $crud = $this->crud_function('','');
            $crud->unset_delete();
            $crud = $crud->render();
            $this->loadView($crud);
        } 
        
        function productos($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);            
            $crud->set_relation_n_n('tallas','productos_tallas','tallas','productos_id','tallas_id','{nombre}');
            $crud->field_type('colores','tags');
            $crud->field_type('genero','dropdown',array('M'=>'Masculino','F'=>'Femenino'));
            $crud->display_as('Colores (Separados por coma)');
            if($crud->getParameters()!='list'){
                $crud->display_as('foto','Foto de listado (Tamaño recomendado 1000x600px)');
            }
            $crud->add_action('<i class="fa fa-image"></i> Adm Fotos','',base_url('productos/admin/productos_fotos').'/');
            $crud->set_field_upload('foto','img/productos');
            $output = $crud->render();
            $this->loadView($output);
        }   

        function pedidos($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);                        
            $crud->add_action('<i class="fa fa-search"></i> Detalles','',base_url('productos/admin/pedidos_detalles').'/');            
            $crud->callback_after_delete(function($primary){get_instance()->db->delete('pedidos_productos',array('pedidos_id'=>$primary));});
            $crud->callback_column('total',function($val,$row){
                return $this->db->query('select sum(precio*cantidad) as total from pedidos_productos where pedidos_id = '.$row->id)->row()->total;
            });
            if($crud->getParameters()=='list'){
                $crud->field_type('status','dropdown',array('1'=>'<span class="label label-danger">Sin atender</span>','2'=>'<span class="label label-success">Atendido</span>'));
            }else{
                $crud->field_type('status','dropdown',array('1'=>'Sin atender','2'=>'Atendido'));
            }
            $output = $crud->render();
            $this->loadView($output);
        }  

        function pedidos_detalles($x = '',$y = ''){
            $this->as['pedidos_detalles'] = 'pedidos_productos';
            $crud = $this->crud_function($x,$y);                        
            $crud->where('productos_id',$x);
            $crud->field_type('productos_id','hidden',$x);
            $crud->set_relation('productos_id','productos','{nombre}');
            $crud->columns('productos_id','tallas_id','cantidad','precio','total');
            $crud->callback_column('total',function($val,$row){return $row->precio*$row->cantidad;});            
            $output = $crud->render();
            $this->loadView($output);
        }  

        function productos_fotos($x = '',$y = ''){
        	$this->load->library('image_crud');
            $crud = new image_crud();
            $crud->set_table('productos_fotos')
            	 ->set_image_path('img/productos')
            	 ->set_url_field('foto')
            	 ->set_relation_field('productos_id')
            	 ->set_ordering_field('orden');
            $crud->module = 'productos';
           	$output = $crud->render();           	
            $this->loadView($output);
        } 

        function importador(){
            $crud = $this->crud_function('','');                        
            $crud->field_type('temporada','enum',array('VERANO','OTOÑO','PRIMAVERA','INVIERNO'));
            $crud->field_type('remplazar','true_false',array('1'=>'SI','0'=>'NO'));
            $crud->set_field_upload('fichero','files');     
            $crud->display_as('fichero','Fichero(Formato permitido XLS)')
                 ->display_as('anio','Año');
            $crud->set_lang_string('insert_success_message','Su fichero ha sido almacenado, deseas procesarlo? <a href="'.base_url('productos/admin/procesar_fichero').'/{primary}">Pulsa aqui</a> |');
            $crud->add_action('<i class="fa fa-file"></i> Procesar Fichero','',base_url('productos/admin/procesar_fichero').'/');
            $output = $crud->render();
            $this->loadView($output);
        }

        function procesar_fichero($id){
            if(is_numeric($id)){
                $fichero = $this->db->get_where('importador',array('id'=>$id));
                if($fichero->num_rows()>0){
                    $fichero = $fichero->row();
                    require_once APPPATH."libraries/Excel/SpreadsheetReader.php";
                    $excel = new SpreadsheetReader('files/'.$fichero->fichero);
                    $almacenados = 0;
                    $total = 0;
                    foreach ($excel as $Row)
                    {                        
                        $genero = $Row[26]=='DAMA'?'F':'M';
                        $precio = str_replace('$','',$Row[2]);
                        $precio = str_replace(',','.',$precio);
                        if(is_numeric($precio)){
                            $total++;
                            $data = array(
                                'temporada'=>$fichero->temporada.'-'.$fichero->anio,
                                'genero'=>$genero,
                                'nombre'=>$Row[1],
                                'estilo'=>$Row[0],
                                'precio'=>$precio,
                                'colores'=>$Row[27],
                                'estatus'=>0,
                                'origen'=>$Row[5],
                                'linea'=>$Row[3],
                                'lineal'=>$Row[4]
                            );
                            $almacenar_tallas = false;
                            $existe = $this->db->get_where('productos',array('temporada'=>$data['temporada'],'estilo'=>$data['estilo']));
                            if($existe->num_rows()>0 && $fichero->reemplazar==1){
                                $this->db->update('productos',$data,array('temporada'=>$data['temporada'],'estilo'=>$data['estilo']));
                                $producto = $existe->row()->id;
                                $almacenados++;
                                $this->db->delete('productos_tallas');
                                $almacenar_tallas = true;
                            }
                            if($existe->num_rows()==0){
                                $this->db->insert('productos',$data);
                                $producto = $this->db->insert_id();
                                $almacenados++;
                                $almacenar_tallas = true;
                            }                            
                            if($almacenar_tallas){
                                for($i=6;$i<=25;$i++){
                                    if(is_numeric($Row[$i])){
                                        $tallaid = $this->db->get_where('tallas',array('nombre'=>$Row[$i]));
                                        if($tallaid->num_rows()>0){
                                            $this->db->insert('productos_tallas',array('productos_id'=>$producto,'tallas_id'=>$tallaid->row()->id));
                                        }
                                    }
                                }
                            }
                        }                        
                    }
                    echo 'Registros insertados '.$almacenados.' de '.$total;
                }
            }
        }
    }
?>
