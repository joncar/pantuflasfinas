<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{        
        function __construct() {
            parent::__construct();
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
        }

        function lista($type = ''){
            $crud = new ajax_grocery_crud();
            $crud->set_table('productos')
            	 ->set_subject('Producto')
            	 ->set_theme('crud')                 
            	 ->unset_add()->unset_edit()->unset_delete()->unset_read()->unset_print();
            $url = !empty($type)?$type.'/':'';
            $crud->set_url('productos/frontend/lista/'.$url);
            //$crud->where('estatus',1);
            if(!empty($type) && ($type=='M' || $type=='F')){
                $crud->where('genero',$type);
            }
            $crud->where('productos.estatus',1);
            $crud->columns('foto','nombre','link');
            $crud->callback_column('link',function($val,$row){
                return base_url('pedidos/'.toUrl($row->id.'-'.$row->nombre));
            });            
            $crud = $crud->render('','application/modules/productos/views/');
            $crud->view = 'listado';
            $this->loadView($crud);
        }
        
        function read($id){
            $id = explode('-',$id);
            $id = $id[0];
            if(is_numeric($id)){
                $productos = new Bdsource();
                $productos->where('id',$id);
                $productos->init('productos',TRUE);
                $this->productos->link = site_url('pedidos/'.toURL($this->productos->id.'-'.$this->productos->nombre));
                $this->productos->foto = base_url('img/productos/'.$this->productos->foto);                
                $this->loadView(
                    array(
                        'view'=>'detail',
                        'detail'=>$this->productos,
                        'title'=>$this->productos->nombre,                        
                    ));
            }else{
                throw new Exception('No se encuentra la entrada solicitada',404);
            }
        }

        function addCart(){       
            if(isset($_POST['pedido']) && count($_POST['pedido'])>0){
                $this->querys->delCarrito($_POST['id']);
                foreach($_POST['pedido'] as $p){
                    $p['precio'] = $_POST['precio'];
                    $p['id'] = $_POST['id'];
                    $this->querys->setCarrito((object)$p,false);                
                }
                redirect('carrito');    
            }else{
                $_SESSION['msj'] = $this->error('Ocurrio un error añadiendo sus productos');
                redirect($_SERVER['HTTP_REFERER']);
            }
            /*if(!empty($_POST['tallas_id'])){
                $this->querys->setCarrito((object)$_POST,false);
                redirect('carrito');
            }else{
                $_SESSION['msj'] = $this->error('Por favor elije la talla que deseas añadir al carro');
                redirect($_SERVER['HTTP_REFERER']);
            }*/
        }

        function delCarrito($id){
            $this->querys->delCarrito($id);
            redirect('carrito');
        }

        function carrito(){            
            $this->loadView('carrito');
        }

        function sendPedido(){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('nombre','Nombre','required');
            $this->form_validation->set_rules('telefono','Telefono','required');
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('empresa','Empresa','required');
            if($this->form_validation->run()){
                if(!empty($_SESSION['carrito'])){
                    unset($_POST['g-recaptcha-response']);
                    $_POST['total'] = 0;
                    $_POST['fecha'] = date("Y-m-d");
                    $this->db->insert('pedidos',$_POST);
                    $id = $this->db->insert_id();
                    foreach($_SESSION['carrito'] as $c){
                        $this->db->insert('pedidos_productos',array(
                            'pedidos_id'=>$id,
                            'productos_id'=>$c->id,
                            'cantidad'=>$c->cantidad,
                            'precio'=>$c->precio,
                            'tallas_id'=>$c->tallas_id
                        ));
                        $_POST['total']+= $c->precio*$c->cantidad;
                    }
                    $this->db->update('pedidos',array('total'=>$_POST['total']),array('id'=>$id));
                    unset($_SESSION['carrito']);
                    $_SESSION['msj'] = $this->success('Su pedido se ha enviado con éxito, pronto le contactaremos');
                    $_POST['id'] = $id;
                    $this->enviarcorreo((object)$_POST,2,$this->db->get('ajustes')->row()->correo_notificacion_pedidos);
                    $this->enviarcorreo((object)$_POST,3,$_POST['email']);
                }else{
                    $_SESSION['msj'] = $this->error($this->form_validation->error_string());    
                }
            }else{
                $_SESSION['msj'] = $this->error($this->form_validation->error_string());
            }

            redirect('carrito');
        }
    }
