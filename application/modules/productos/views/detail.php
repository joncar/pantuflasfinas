<?php 
    $page = $this->load->view('producto',array(),TRUE,'paginas');        

    $page = str_replace('[contacto]',$this->load->view('contacto',array(),TRUE),$page); 
	$response = empty($_SESSION['msj'])?'':$_SESSION['msj'];
	$page = str_replace('[response]',$response,$page); 
	$fotos = $this->db->get_where('productos_fotos',array('productos_id'=>$detail->id));
	foreach($fotos->result() as $n=>$f){
		$fotos->row($n)->foto = base_url().'img/productos/'.$f->foto;
	}
	$page = $this->querys->fillFields($page,array('fotos'=>$fotos));
	foreach($detail as $n=>$v){		
		$page = str_replace('['.$n.']',$v,$page); 
	}
	$this->db->select('tallas.*');
	$this->db->join('tallas','tallas.id = productos_tallas.tallas_id');
	$this->db->where('productos_id',$detail->id);
	$this->db->order_by('tallas.nombre','ASC');
	$page = str_replace('[tallas]',form_dropdown_from_query('tallas_id','productos_tallas','id','nombre',0,'id="talla"',FALSE),$page); 	
	$page = str_replace('[form_url]',base_url('productos/frontend/addCart'),$page); 	

	$telasstring = '<div class="col-md-12">
						<p><strong>Telas disponibles:</strong></p>
					</div>

					<div class="col-md-12 mb-3">[telas]</div>';
	if(!empty($detail->tipos_telas_id)){		
		$telas = $this->db->get_where('telas',array('tipos_telas_id'=>$detail->tipos_telas_id));
		$colores = explode(',',$detail->colores_tela);		
		$cont = '<div class="row">';
		foreach($telas->result() as $t){
			if(in_array($t->id,$colores)){
				$cont.= '<div class="col-md-3 mb-3 mb-md-3"><img style="width:100%; height:auto" class="zoom_telas" class="img-fluid d-block m-auto" data-zoom-image="'.base_url('img/telas/'.$t->imagen).'" src="'.base_url('img/telas/'.$t->imagen).'" alt="'.$t->nombre.'"></div>';
			}
		}
		$cont.= '</div>';
		$telasstring = str_replace('[telas]',$cont,$telasstring); 	
		$page = str_replace('[telas]',$telasstring,$page); 	
	}else{
		$page = str_replace('[telas]','',$page); 	
	}

	if(!empty($_SESSION['msj'])){
		$page = str_replace('[msj]',$_SESSION['msj'],$page); 	
		unset($_SESSION['msj']);
	}else{
		$page = str_replace('[msj]','',$page); 	
	}

	$page = str_replace('[whatsapp]',$this->db->get('ajustes')->row()->whatsapp,$page); 

    echo $page;
?>
<script>
	var ws = $(window).width();
	if( ws>767 ) {
		$(".zoom_telas").elevateZoom({zoomWindowWidth:500, zoomWindowHeight:500, zoomType:"inner", responsive:true, scrollZoom:true});		
	} else {
		$(".zoom_telas").elevateZoom({zoomWindowWidth:500, zoomWindowHeight:500, zoomType:"inner", responsive:true, scrollZoom:true});		
	}

	var addeds = <?php 
		$addeds = array();
		if(!empty($_SESSION['carrito'])){
			foreach($_SESSION['carrito'] as $c){
				if($c->id==$detail->id){
					$addeds[] = array(
						'cantidad'=>$c->cantidad,
						'talla'=>$c->tallas_id,
						'label'=>$this->db->get_where('tallas',array('id'=>$c->tallas_id))->row()->nombre
					);
				}
			}
		}
		echo json_encode($addeds);
	?>;

	$("#addPedido").on('click',function(){
		if($("#talla").val()!=='' && !isNaN(parseInt($("#qty-product").val()))){			
			var encontrado = false;
			for(var i in addeds){
				if(addeds[i].talla==$("#talla").val()){
					addeds[i].cantidad = $("#qty-product").val();
					encontrado = true;
				}
			}
			if(!encontrado){
				addeds.push({
					cantidad:$("#qty-product").val(),
					talla:$("#talla").val(),
					label:$("#talla option:selected").html()
				});
			}
			refresh();
		}
	});

	function refresh(){
		if(addeds.length===0){
			$(".itemsaddeds").hide();
		}else{
			$(".itemsaddeds").show();
		}
		var tr = '';
		var inputs = '';
		var total = 0;
		for(var i in addeds){
			tr += '<tr><td>'+addeds[i].cantidad+'</td><td>'+addeds[i].label+'</td><td style="text-align: center"><a href="javascript:rem(\''+addeds[i].talla+'\')" style="color:red">x</a></td></tr>';
			inputs+= '<input type="hidden" name="pedido['+i+'][cantidad]" value="'+addeds[i].cantidad+'">';
			inputs+= '<input type="hidden" name="pedido['+i+'][tallas_id]" value="'+addeds[i].talla+'">';
			total+= parseInt(addeds[i].cantidad);
		}
		$("#itemsProducts").html(tr);
		$("#hiddens").html(inputs);
		if(total>=60){
			$("#SendButtonSubmit").attr('disabled',false);
		}else{
			$("#SendButtonSubmit").attr('disabled',true);
		}
	}

	function rem(talla){
		var aux = [];
		for(var i in addeds){
			if(addeds[i].talla!==talla){
				aux.push(addeds[i]);
			}
		}
		addeds = aux;
		refresh();
	}

	refresh();
</script>