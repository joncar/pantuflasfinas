<?php
$this->set_css('assets/public_themes/productos/css/flexigrid.css');
$this->set_js_lib($this->default_javascript_path . '/' . grocery_CRUD::JQUERY);
$this->set_js_lib($this->default_javascript_path . '/jquery_plugins/jquery.noty.js');
$this->set_js_lib($this->default_javascript_path . '/jquery_plugins/config/jquery.noty.config.js');
$this->set_js('assets/public_themes/productos/js/cookies.js');
$this->set_js('assets/public_themes/productos/js/flexigrid.js');
$this->set_js('assets/public_themes/productos/js/jquery.form.js');
$this->set_js($this->default_javascript_path . '/jquery_plugins/jquery.numeric.min.js');
$this->set_js('assets/public_themes/productos/js/jquery.printElement.min.js');
$this->set_js('assets/public_themes/productos/js/pagination.js');
/** Fancybox */
$this->set_css($this->default_css_path . '/jquery_plugins/fancybox/jquery.fancybox.css');
$this->set_js($this->default_javascript_path . '/jquery_plugins/jquery.fancybox-1.3.4.js');
$this->set_js($this->default_javascript_path . '/jquery_plugins/jquery.easing-1.3.pack.js');

/** Jquery UI */
$this->load_js_jqueryui();
?>
<script type='text/javascript'>
    var base_url = '<?php echo base_url(); ?>';
    var subject = '<?php echo $subject ?>';
    var ajax_list_info_url = '<?php echo $ajax_list_info_url; ?>';
    var ajax_list = '<?php echo $ajax_list_url; ?>';
    var unique_hash = '<?php echo $unique_hash; ?>';
    var message_alert_delete = "<?php echo $this->l('alert_delete'); ?>";
    var crud_pagin = 1;
    var fragmentos = 1;
    var total_results = <?= $total_results ?>;
</script>

<?php echo form_open($ajax_list_url, 'method="post" id="filtering_form" class="filtering_form" autocomplete = "off" data-ajax-list-info-url="' . $ajax_list_info_url . '" onsubmit="return filterSearchClick(this)"'); ?>
<div class="flexigrid" style='width: 100%;' data-unique-hash="<?php echo $unique_hash; ?>">        
    <section class="bg-yellow-light pt-60 pb-60">
        <div class="container-custom">
            <div class="row" style="margin:20px">
                  <div class="col-xs-12 col-sm-9"></div>
                  <div class="col-xs-12 col-sm-3">
                    <label>Buscar:
                        <input name="search_field[]" value="estilo" type="hidden">
                        <input class="form-control" name="search_text[]" value="" placeholder="Busca por estilo" type="search">
                    </label>
                  </div>
            </div>
            <div class="row ajax_list">
                <?php echo $list_view ?>                
            </div>
            <div class="row">
                <div class="col-md-12 mt-3 mt-md-5">
                  <ul class="pagination pagination-black justify-content-center">                        
                  </ul>
                </div>
            </div>
        </div>
    </section>
    <button  class="ajax_refresh_and_loading" style="display: none"></button>
</div>
<input type='hidden' name='order_by[0]' class='hidden-sorting' value='<?php if (!empty($order_by[0])) { ?><?php echo $order_by[0] ?><?php } ?>' />
<input type='hidden' name='order_by[1]' class='hidden-ordering'  value='<?php if (!empty($order_by[1])) { ?><?php echo $order_by[1] ?><?php } ?>'/>
<input type='hidden' name='per_page' value='10'/>
<?php echo form_close() ?>
