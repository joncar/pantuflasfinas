<?php 
    $page = $this->load->view('cart',array(),TRUE,'paginas');        

    $page = str_replace('[contacto]',$this->load->view('contacto',array(),TRUE),$page); 
	$response = empty($_SESSION['msj'])?'':$_SESSION['msj'];
	unset($_SESSION['msj']);
	$page = str_replace('[response]',$response,$page); 
	
	$carrito = array();
	$total = 0;
	if(isset($_SESSION['carrito'])){
		foreach($_SESSION['carrito'] as $car){
			$producto = $this->db->get_where('productos',array('id'=>$car->id));
			if($producto->num_rows()>0){
				foreach($producto->row() as $n=>$v){
					$car->{$n} = $v;
				}
				$car->foto = base_url().'img/productos/'.$car->foto;
				$car->subtotal = $car->precio*$car->cantidad;
				$car->del_link = 'javascript:del('.$car->id.')';			
				$car->link = base_url('pedidos/'.toUrl($car->id.'-'.$car->nombre));			
				$car->talla = $this->db->get_where('tallas',array('id'=>$car->tallas_id))->row()->nombre;
				$total+= $car->subtotal;
				$carrito[] = $car;
			}
		}
	}
	$page = str_replace('[total]',$total,$page); 	
	
	if(count($carrito)==0){
		$page = str_replace('[emptycarmessage]','Su carrito se encuentra vacio',$page); 	
	}else{
		$page = str_replace('[emptycarmessage]','',$page); 	
	}

	$page = str_replace('[pedidoslink]',base_url('pedidos'),$page); 	
	$page = str_replace('[formpedidosendlink]',base_url('productos/frontend/sendPedido'),$page); 
	$page = $this->querys->fillFields($page,array('carrito'=>$carrito));

	$page = str_replace('[whatsapp]',$this->db->get('ajustes')->row()->whatsapp,$page); 

    echo $page;
?>
<script>
	function del(id){
		if(confirm('Estas seguro que deseas eliminar de la lista todos los productos con este estilo?, si solo deseas eliminar una talla puedes hacerlo desde el boton de cambiar cantidad')){
			document.location.href="<?= base_url() ?>productos/frontend/delCarrito/"+id;
		}
	}
</script>