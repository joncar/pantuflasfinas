<?= $output ?>
<script>
	var colores = <?= json_encode(explode(',',$colores)) ?>;
	$(document).on('change','#field-tipos_telas_id',function(){
		if($(this).val()!==''){
			$("#colores_tela_field_box").show();
			$.post('<?= base_url('mantenimiento/admin/telas/json_list') ?>',{
				'search_field[]':'tipos_telas_id',
				'search_text[]':$(this).val()
			},function(data){
				data = JSON.parse(data);
				var html = '';
				for(var i in data){
					var sel = colores.indexOf(data[i].id)>=0?'selected':'';
					html+= '<option value="'+data[i].id+'" '+sel+'>'+data[i].nombre+'</option>';
				}
				$("#field-colores_tela").html(html);
				$("#field-colores_tela").chosen().trigger('liszt:updated');
			});
		}else{
			$("#colores_tela_field_box").hide();
		}
	});

	$('#field-tipos_telas_id').trigger('change');
</script>