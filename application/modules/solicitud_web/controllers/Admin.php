<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }

        function solicitud_web(){
        	$this->as['solicitud_web'] = 'pedidos';  
        	$crud = $this->crud_function('','');    
        	$crud->set_subject('Solicitud de pedido');                 
            $crud->add_action('<i class="fa fa-search"></i> Productos','',base_url('solicitud_web/admin/pedidos_detalles').'/');            
            $crud->callback_after_delete(function($primary){get_instance()->db->delete('pedidos_productos',array('pedidos_id'=>$primary));});
            $crud->callback_column('total',function($val,$row){
                return $this->db->query('select sum(precio*cantidad) as total from pedidos_productos where pedidos_id = '.$row->id)->row()->total;
            });
            $crud->columns('id','nombre','telefono','email','total','atendido_por','fecha_atencion','status');
            $crud->display_as('id','#Pedido');
            if($crud->getParameters()=='list'){
                $crud->field_type('status','dropdown',array('1'=>'<span class="label label-danger">Sin atender</span>','2'=>'<span class="label label-success">Atendido</span>'));
                $crud->set_relation('atendido_por','user','nombre');
            }else{            
                $crud->field_type('status','dropdown',array('1'=>'Sin atender','2'=>'Atendido'));
            }
            if($crud->getParameters()=='read'){
                $crud->field_type('fecha_atencion','string');
                $crud->set_relation('atendido_por','user','nombre');
                $crud->callback_field('status',function($val,$row){
                    return $val=='1'?'Sin atender':'Atendido';
                });
            }else{
                $crud->field_type('fecha_atencion','hidden',date("Y-m-d H:i:s"));
                $crud->field_type('atendido_por','hidden',$this->user->id);
            }               
            $crud->unset_delete();
            if($crud->getParameters()=='edit'){
                $crud->edit_fields('comentarios','status','atendido_por','fecha_atencion');
                $crud->required_fields('comentarios','status','atendido_por','fecha_atencion');
            }
            $crud->callback_after_update(function($post,$primary){
                $datos = get_instance()->db->get_where('pedidos')->row();
                if($datos->status==2){
                    $this->enviarcorreo($datos,4);
                }
            });
            $crud->unset_add()->unset_print()->unset_export();
            $output = $crud->render();
            $output->title = 'Solicitudes de pedidos';
            $this->loadView($output);
        }  

        function pedidos_detalles($x = '',$y = ''){
            $this->as['pedidos_detalles'] = 'pedidos_productos';
            $crud = $this->crud_function($x,$y);                        
            $crud->where('pedidos_id',$x);
            $crud->field_type('productos_id','hidden',$x);
            $crud->set_relation('productos_id','productos','{foto}|{estilo}|{nombre}');            
            $crud->callback_column('total',function($val,$row){return $row->precio*$row->cantidad;});            
            $crud->columns('j4b04c546.foto','j4b04c546.estilo','j4b04c546.nombre','tallas_id','cantidad','precio','total');
            $crud->callback_column('j4b04c546.foto',function($val,$row){return '<img src="'.base_url('img/productos/'.explode('|',$row->s4b04c546)[0]).'" style="width:50px">';})
                 ->callback_column('j4b04c546.estilo',function($val,$row){return explode('|',$row->s4b04c546)[1];})
                 ->callback_column('j4b04c546.nombre',function($val,$row){return explode('|',$row->s4b04c546)[2];})
                 ->display_as('j4b04c546.foto','Imagen')
                 ->display_as('j4b04c546.estilo','Estilo')
                 ->display_as('j4b04c546.nombre','Nombre')
                 ->display_as('tallas_id','Talla');
            $crud->unset_delete()
                 ->unset_edit()
                 ->unset_read()
                 ->unset_add();
            $output = $crud->render();
            $output->title = 'Detalle de pedido';
            $this->loadView($output);
        }
    }
?>
