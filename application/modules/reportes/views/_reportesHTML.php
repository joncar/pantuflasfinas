<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="heading1">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
                    General
                </a>
            </h4>
        </div>
        <div id="collapse1" data-url="ventas_dia" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-4">
                        Articulos cargados: <?= $this->db->get('productos')->num_rows() ?>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        Total contactenos: <?= $this->db->get('contactenos')->num_rows() ?>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        Total pedidos: <?= $this->db->get('pedidos')->num_rows() ?>
                    </div>
                    
                </div>
                
            </div>
        </div>
    </div>


    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="heading1">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">
                    Pedidos
                </a>
            </h4>
        </div>
        <div id="collapse2" data-url="ventas_dia" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-4">
                        Recibidos: <?= $this->db->get('pedidos')->num_rows() ?>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        Pendientes: <?= $this->db->get_where('pedidos',array('status'=>1))->num_rows() ?>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        Atendidos: <?= $this->db->get_where('pedidos',array('status'=>2))->num_rows() ?>
                    </div>
                    
                </div>
                
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="heading1">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapse3">
                    Contactos
                </a>
            </h4>
        </div>
        <div id="collapse3" data-url="ventas_dia" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-4">
                        Recibidos: <?= $this->db->get('contactenos')->num_rows() ?>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        Pendientes: <?= $this->db->get_where('contactenos',array('estatus'=>0))->num_rows() ?>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        Atendidos: <?= $this->db->get_where('contactenos',array('estatus'=>1))->num_rows() ?>
                    </div>
                    
                </div>
                
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="heading1">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="true" aria-controls="collapse4">
                    Más solicitado
                </a>
            </h4>
        </div>
        <div id="collapse4" data-url="ventas_dia" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
            <div class="panel-body">                
                <?= $output ?>
            </div>
        </div>
    </div>
    
</div>