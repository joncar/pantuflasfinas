<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }

        function contacto_web(){
        	$this->as['contacto_web'] = 'contactenos';
            $crud = $this->crud_function('','');
            $crud->set_subject('Solicitud de contacto');
            if($crud->getParameters()=='list'){
            	$crud->field_type('estatus','true_false',array('0'=>'<span class="label label-danger">Sin atender</span>','1'=>'<span class="label label-success">Atendido</span>'));
            }else{
            	$crud->field_type('estatus','true_false',array('0'=>'Sin atender','1'=>'Atendido'));
        	}
            if($crud->getParameters()=='edit'){
                $crud->field_type('atendido_por','hidden',$this->user->id);
                $crud->edit_fields('comentarios','atendido_por','estatus','fecha_atencion');                
            }            
            if($crud->getParameters()=='list'){
                $crud->set_relation('atendido_por','user','nombre');
            }

            $crud->unset_add();
            $crud->callback_after_update(function($post,$primary){
                $datos = get_instance()->db->get_where('contactenos')->row();
                if($datos->estatus==1){
                    $this->enviarcorreo($datos,4);
                }
            });
            $crud->field_type('fecha_atencion','hidden',date("Y-m-d H:i:s"));

            $crud->columns('id','nombre','email','atendido_por','fecha_atencion','estatus');
            $crud->display_as('id','#Solicitud');
            $crud->unset_delete();
            $crud = $crud->render();
            $crud->title = 'Solicitudes de contacto';
            $this->loadView($crud);
        }
    }
?>
